package com.example.domain.entity.teams

data class TeamsDataEntity(
    val _id: String?,
    val fifa_code: String?,
    val flag: String?,
    val groups: String?,
    val id: String?,
    val iso2: String?,
    val name_en: String?,
    val name_fa: String?
)