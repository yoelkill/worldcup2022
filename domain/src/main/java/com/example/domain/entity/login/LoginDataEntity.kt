package com.example.domain.entity.login

data class LoginDataEntity(
    val token: String?
)