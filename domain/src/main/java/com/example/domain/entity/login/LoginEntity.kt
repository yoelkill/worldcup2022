package com.example.domain.entity.login

data class LoginEntity(
    val data: LoginDataEntity?,
    val status: String?
)