package com.example.domain.entity.matches

data class MatchesEntity(
    val data: List<MatchesDataEntity>?,
    val status: String?
)