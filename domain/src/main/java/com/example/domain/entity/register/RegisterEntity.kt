package com.example.domain.entity.register

data class RegisterEntity(
    val data: RegisterDataEntity?,
    val message: String?,
    val status: String?
)