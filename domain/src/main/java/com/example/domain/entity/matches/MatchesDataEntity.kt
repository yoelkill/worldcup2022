package com.example.domain.entity.matches

data class MatchesDataEntity(
    val _id: String?,
    val away_flag: String?,
    val away_score: Int?,
    val away_scorers: List<String?>?,
    val away_team_en: String?,
    val away_team_fa: String?,
    val away_team_id: String?,
    val finished: String?,
    val group: String?,
    val home_flag: String?,
    val home_score: Int?,
    val home_scorers: List<String?>?,
    val home_team_en: String?,
    val home_team_fa: String?,
    val home_team_id: String?,
    val id: String?,
    val local_date: String?,
    val matchday: String?,
    val persian_date: String?,
    val stadium_id: String?,
    val time_elapsed: String?,
    val type: String?
)