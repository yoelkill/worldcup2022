package com.example.domain.entity.register

data class RegisterDataEntity(
    val token: String?
)