package com.example.domain.entity.teams

data class TeamsEntity(
    val data: List<TeamsDataEntity>?,
    val status: String?
)