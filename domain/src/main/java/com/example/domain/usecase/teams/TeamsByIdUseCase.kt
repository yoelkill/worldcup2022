package com.example.domain.usecase.teams

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity

interface TeamsByIdUseCase {
    suspend fun getTeamsById(id: String): Either<Failure, TeamsEntity>
}