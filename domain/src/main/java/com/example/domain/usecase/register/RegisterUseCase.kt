package com.example.domain.usecase.register

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.register.RegisterEntity

interface RegisterUseCase {
    suspend fun register(
        name: String,
        email: String,
        pwr: String,
        pwrConfirm: String
    ): Either<Failure, RegisterEntity>
}