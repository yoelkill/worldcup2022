package com.example.domain.usecase.login

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.login.LoginEntity

interface LoginUseCase {
    suspend fun getLogin(email:String,password:String): Either<Failure, LoginEntity>
}