package com.example.domain.usecase.matches

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity

interface MatchesUseCase {
    suspend fun getMatches(): Either<Failure,MatchesEntity>
}