package com.example.domain.usecase.matches

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity

interface MatchesByDateUseCase {
    suspend fun getMatchesByDate(date:String): Either<Failure, MatchesEntity>
}