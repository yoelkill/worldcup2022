package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.login.LoginEntity

interface LoginRepository {
    suspend fun getLogin(email:String,password:String): Either<Failure,LoginEntity>
}