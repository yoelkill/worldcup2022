package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity

interface TeamsByIdRepository {
    suspend fun getTeamsById(id: String): Either<Failure, TeamsEntity>
}