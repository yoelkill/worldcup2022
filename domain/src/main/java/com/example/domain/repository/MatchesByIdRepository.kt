package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity

interface MatchesByIdRepository {
    suspend fun getMatchesById(id: String): Either<Failure, MatchesEntity>
}