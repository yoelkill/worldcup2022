package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.register.RegisterEntity

interface RegisterRepository {
    suspend fun getRegister(
        name: String,
        email: String,
        pwr: String,
        pwrConfirm: String
    ): Either<Failure, RegisterEntity>
}