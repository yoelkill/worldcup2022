package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity

interface MatchesByDateRepository {
    suspend fun getMatchesByDate(date:String): Either<Failure, MatchesEntity>
}