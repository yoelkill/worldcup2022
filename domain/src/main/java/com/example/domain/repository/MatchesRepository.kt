package com.example.domain.repository

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity

interface MatchesRepository {
    suspend fun getMatches(): Either<Failure, MatchesEntity>
}