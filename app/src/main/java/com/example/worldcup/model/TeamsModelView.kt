package com.example.worldcup.model

data class TeamsModelView(
    val id: String,
    val name_en: String,
    val flag: String,
    val fifa_code: String,
    val groups: String
)