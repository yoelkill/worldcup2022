package com.example.worldcup.di

import com.example.worldcup.mapper.matches.MatchesModelMapper
import com.example.worldcup.mapper.matches.MatchesModelMapperImpl
import com.example.worldcup.mapper.teams.TeamsModelMapper
import com.example.worldcup.mapper.teams.TeamsModelMapperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class MapperModule {

    @ViewModelScoped
    @Binds
    abstract fun bindMapperMatches(
        mapperImpl: MatchesModelMapperImpl
    ): MatchesModelMapper

    @ViewModelScoped
    @Binds
    abstract fun bindMapperTeams(
        mapperImpl: TeamsModelMapperImpl
    ): TeamsModelMapper

}