package com.example.worldcup.feautures.ui.register

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.adapters.MatchesAdapter
import com.example.worldcup.databinding.FragmentRegisterBinding
import com.example.worldcup.feautures.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding, RegisterViewModel>() {
    override val getLayoutId: Int
        get() = R.layout.fragment_register
    override val getBindingVariable: Int
        get() = BR.registerViewModel
    override val classTypeOfVM: Class<RegisterViewModel>
        get() = RegisterViewModel::class.java

    override fun onFragmentViewReady(view: View) {
        configView()
    }

    private fun configView() {
        binding.buttonRegister.setOnClickListener {
            if (validateInput()) {
                myViewModel.executeRegister(
                    binding.textName.text.toString(),
                    binding.textEmail.text.toString(),
                    binding.textPassword.text.toString(),
                    binding.textPasswordConfirm.text.toString()
                )
            }
        }
    }

    private fun validateInput(): Boolean {
        if (binding.textName.text.toString().isEmpty()) {
            showToast(getString(R.string.validate_input_name))
            return false
        } else if (binding.textEmail.text.toString().isEmpty()) {
            showToast(getString(R.string.validate_input_email))
            return false
        } else if (binding.textPassword.text.toString().isEmpty()) {
            showToast(getString(R.string.validate_input_pswr))
            return false
        } else if (binding.textPasswordConfirm.text.toString().isEmpty()) {
            showToast(getString(R.string.validate_input_psw_confirm))
            return false
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            myViewModel.state.collectLatest { state ->
                when (state) {
                    is UIRegisterState.OnError -> {
                        showToast(state.msg)
                    }
                    is UIRegisterState.OnSuccess -> {
                        clearField()
                        findNavController().navigate(R.id.listMatchesFragment)
                    }
                    else -> {}
                }
            }
        }
    }
    private fun clearField(){
        binding.textName.text.clear()
        binding.textEmail.text.clear()
        binding.textPassword.text.clear()
        binding.textPasswordConfirm.text.clear()
    }

}