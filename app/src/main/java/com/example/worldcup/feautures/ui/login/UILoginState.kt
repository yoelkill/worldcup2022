package com.example.worldcup.feautures.ui.login

sealed class UILoginState{
    object OnInit : UILoginState()
    object OnSuccess: UILoginState()
    data class OnError(val msg: String): UILoginState()
}
