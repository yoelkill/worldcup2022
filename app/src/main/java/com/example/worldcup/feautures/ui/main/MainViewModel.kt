package com.example.worldcup.feautures.ui.main

import com.example.worldcup.feautures.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(): BaseViewModel<Any>() {
}