package com.example.worldcup.feautures.ui.teams

import androidx.lifecycle.*
import com.example.domain.entity.register.RegisterEntity
import com.example.domain.entity.teams.TeamsEntity
import com.example.domain.usecase.teams.TeamsByIdUseCase
import com.example.worldcup.feautures.base.BaseViewModel
import com.example.worldcup.feautures.ui.register.UIRegisterState
import com.example.worldcup.mapper.teams.TeamsModelMapper
import com.example.worldcup.model.TeamsModelView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class TeamViewModel @Inject constructor(
    private val teamsByIdUseCase: TeamsByIdUseCase,
    private val mapper: TeamsModelMapper
) :
    BaseViewModel<Any>() {
    private val _teams = MutableLiveData<TeamsEntity>()
    val teams: LiveData<List<TeamsModelView>?> = _teams.switchMap {
        liveData(Dispatchers.IO) {
            emit(mapper.teamDomainToPresentation(it))
        }
    }

    fun executeTeamById(id: String) {
        showLoading(true)
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                teamsByIdUseCase.getTeamsById(id)
            }
            result.either(::handleUseCaseFailureFromBase, ::handleSuccessTeamById)
        }
    }

    private fun handleSuccessTeamById(data: TeamsEntity) {
        showLoading(false)
        _teams.value = data
    }
}