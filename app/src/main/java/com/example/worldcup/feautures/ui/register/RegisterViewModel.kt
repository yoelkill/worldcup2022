package com.example.worldcup.feautures.ui.register

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.entity.Failure
import com.example.domain.entity.login.LoginEntity
import com.example.domain.entity.register.RegisterEntity
import com.example.domain.usecase.register.RegisterUseCase
import com.example.worldcup.feautures.base.BaseViewModel
import com.example.worldcup.feautures.ui.login.UILoginState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(private val registerUseCase: RegisterUseCase) :
    BaseViewModel<Any>() {
    private val _state = MutableSharedFlow<UIRegisterState>()
    val state = _state.asSharedFlow()
    fun executeRegister(name: String, email: String, password: String, passwordConfirm: String) {
        showLoading(true)
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                registerUseCase.register(name, email, password, passwordConfirm)
            }
            result.either(::handleFailure, ::handleSuccessRegister)
        }
    }

    private fun handleFailure(failure: Failure) {
        Log.d("DNI-VM",failure.toString())
        showLoading(false)
        viewModelScope.launch {
            _state.emit(UIRegisterState.OnError(failure.toString()))
        }
    }

    private fun handleSuccessRegister(data: RegisterEntity) {
        showLoading(false)

        viewModelScope.launch {
            if (data.status.equals("success")) {
                _state.emit(UIRegisterState.OnSuccess)
            } else {
                _state.emit(UIRegisterState.OnError(data.message.orEmpty()))
            }
        }
    }
}