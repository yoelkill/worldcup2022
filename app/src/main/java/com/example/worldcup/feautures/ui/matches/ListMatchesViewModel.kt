package com.example.worldcup.feautures.ui.matches


import androidx.lifecycle.*
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.usecase.matches.MatchesUseCase
import com.example.worldcup.feautures.base.BaseViewModel
import com.example.worldcup.feautures.ui.register.UIRegisterState
import com.example.worldcup.mapper.matches.MatchesModelMapper
import com.example.worldcup.model.MatchesModelView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ListMatchesViewModel @Inject constructor(
    private val matchesUseCase: MatchesUseCase,
    private val mapper: MatchesModelMapper
) :
    BaseViewModel<Any>() {
    private val _state = MutableSharedFlow<UIMatchesState>()
    val state = _state.asSharedFlow()
    private val _listMatches = MutableLiveData<MatchesEntity>()
    val listMatches: LiveData<List<MatchesModelView>?> = _listMatches.switchMap {
        liveData(Dispatchers.IO) {
            emit(mapper.matchesDomainToPresentation(it))
        }
    }
    init {
        executeUseCaseGetMatches()
    }
    fun executeUseCaseGetMatches(){
        showLoading(true)
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO){
                matchesUseCase.getMatches()
            }
            result.either(::handleUseCaseFailureFromBase,::handleSuccessMatches)
        }
    }

    private fun handleSuccessMatches(matches: MatchesEntity){
        showLoading(false)
        _listMatches.value = matches
        viewModelScope.launch {
            if (matches.status.equals("success")) {
                _state.emit(UIMatchesState.OnSuccess)
            }else if(matches.data.isNullOrEmpty()) {
                _state.emit(UIMatchesState.OnListIsEmpty)
            }
        }

    }
}