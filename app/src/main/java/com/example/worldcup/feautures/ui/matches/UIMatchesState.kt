package com.example.worldcup.feautures.ui.matches

sealed class UIMatchesState {
    object OnInit :UIMatchesState()
    object OnSuccess : UIMatchesState()
    object OnListIsEmpty :UIMatchesState()
}