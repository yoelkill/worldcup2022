package com.example.worldcup.feautures.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.worldcup.feautures.dialog.LoadingDialog
import dagger.hilt.android.AndroidEntryPoint
import kotlin.reflect.KClass

//@Module
//@InstallIn(ActivityComponent::class)
//@AndroidEntryPoint
abstract class BaseActivity<T : ViewDataBinding, VM : BaseViewModel<*>>: AppCompatActivity() {

    lateinit var binding: T
    private var myDialog: LoadingDialog? = null

    abstract val getLayoutId: Int
    abstract val getBindingVariable: Int
    abstract val classTypeOfVM: Class<VM>

    lateinit var myViewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myViewModel = ViewModelProvider(this).get(classTypeOfVM)
        if (myViewModel.equals(null)) {
            throw Exception("View Model must not be null.")
        }

        binding = DataBindingUtil.setContentView(this, getLayoutId)
        binding.setVariable(getBindingVariable, myViewModel)
        binding.lifecycleOwner = this
        binding.executePendingBindings()

        myDialog = LoadingDialog()

        showLoadingDialog()
    }

    private fun showLoadingDialog() {
        myViewModel.isLoading.observe(this, {
            try {
                if (it) {
                    myDialog?.let { dialog ->
                        if (!dialog.isAdded) {
                            dialog.show(supportFragmentManager, "loading")//childFragmentManager
                        } else {

                        }
                    }

                } else if (!it) {
                    myDialog?.dismiss()
                }
            }catch (e: Exception){

            }

        })
    }
}

