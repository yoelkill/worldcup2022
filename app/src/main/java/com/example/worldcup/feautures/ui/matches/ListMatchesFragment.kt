package com.example.worldcup.feautures.ui.matches

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.adapters.MatchesAdapter
import com.example.worldcup.databinding.FragmentListMatchesBinding
import com.example.worldcup.feautures.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListMatchesFragment : BaseFragment<FragmentListMatchesBinding,ListMatchesViewModel>() {
    private val adapterMatches = MatchesAdapter(mutableListOf())
    override val getLayoutId: Int
        get() = R.layout.fragment_list_matches
    override val getBindingVariable: Int
        get() = BR.listMatchesViewModel
    override val classTypeOfVM: Class<ListMatchesViewModel>
        get() = ListMatchesViewModel::class.java

    override fun onFragmentViewReady(view: View) {
        binding.rvMatches.layoutManager = LinearLayoutManager(binding.rvMatches.context)
        binding.rvMatches.adapter = adapterMatches

        this.lifecycleScope.launchWhenStarted {
            myViewModel.state.collect { state ->
                when(state){
                    is UIMatchesState.OnInit -> {}
                    is UIMatchesState.OnListIsEmpty -> {
                        showToast("Lista vacia")
                    }
                    else -> {}
                }
            }
        }
        binding.myToolbar.ibBack.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.myToolbar.tvCloseSession.setOnClickListener {
            findNavController().popBackStack(R.id.loginFragment,false)
        }
        myViewModel.listMatches.observe(this.viewLifecycleOwner){ listMatches ->
            listMatches?.let {
                adapterMatches.addItems(it)
            }
        }
    }


}