package com.example.worldcup.feautures.ui.login

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.databinding.FragmentLoginBinding
import com.example.worldcup.feautures.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding,LoginViewModel>() {

    override val getLayoutId: Int
        get() = R.layout.fragment_login
    override val getBindingVariable: Int
        get() = BR.loginViewModel
    override val classTypeOfVM: Class<LoginViewModel>
        get() = LoginViewModel::class.java

    override fun onFragmentViewReady(view: View) {
        configView()
    }
    private fun configView(){
      binding.buttonLogin.setOnClickListener {
          if(validateInput()) myViewModel.executeLogin(binding.textLogin.text.toString(),binding.textPassword.text.toString())
      }
        binding.buttonRegister.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }
    }

    private fun validateInput():Boolean{
        if(binding.textLogin.text.toString().isEmpty()){
            showToast(getString(R.string.validate_input_email))
            return false
        }else if(binding.textPassword.text.toString().isEmpty()){

            showToast(getString(R.string.validate_input_pswr))
            return false
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            myViewModel.state.collectLatest {state ->
                when(state) {
                    is UILoginState.OnError -> {
                       showToast(state.msg)
                    }
                    is UILoginState.OnSuccess->{
                        clearField()
                        findNavController().navigate(R.id.listMatchesFragment)
                    }
                    else -> {}
                }
            }
        }
    }
    private fun clearField(){
        binding.textLogin.text.clear()
        binding.textPassword.text.clear()
    }
}