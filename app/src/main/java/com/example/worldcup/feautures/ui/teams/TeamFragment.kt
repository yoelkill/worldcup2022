package com.example.worldcup.feautures.ui.teams

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.databinding.FragmentTeamBinding
import com.example.worldcup.feautures.base.BaseFragment
import com.example.worldcup.feautures.utils.Constants
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TeamFragment : BaseFragment<FragmentTeamBinding,TeamViewModel>() {

    override val getLayoutId: Int
        get() = R.layout.fragment_team
    override val getBindingVariable: Int
        get() = BR.teamsViewModel
    override val classTypeOfVM: Class<TeamViewModel>
        get() = TeamViewModel::class.java
    private val code by lazy { arguments?.get(Constants.KEY_CODE_TEAM) as String?:"2"}
    override fun onFragmentViewReady(view: View) {
        binding.myToolbar.ibBack.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.myToolbar.tvCloseSession.setOnClickListener {
            findNavController().popBackStack(R.id.loginFragment,false)
        }
        myViewModel.executeTeamById(code)
        myViewModel.teams.observe(this.viewLifecycleOwner){ list ->
            list?.let {
                binding.tvDescrip.text = getString(R.string.lorem_text)
                binding.teams = it.get(0)
            }
        }
    }

}