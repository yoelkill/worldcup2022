package com.example.worldcup.feautures.ui.register

sealed class UIRegisterState {
    object OnInit : UIRegisterState()
    object OnSuccess : UIRegisterState()
    data class OnError(val msg: String) : UIRegisterState()
}