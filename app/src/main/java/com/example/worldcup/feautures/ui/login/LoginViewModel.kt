package com.example.worldcup.feautures.ui.login


import androidx.lifecycle.viewModelScope
import com.example.domain.entity.login.LoginEntity
import com.example.domain.usecase.login.LoginUseCase
import com.example.worldcup.feautures.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val loginUseCase: LoginUseCase) :
    BaseViewModel<Any>() {
    private val _state = MutableSharedFlow<UILoginState>()
    val state = _state.asSharedFlow()
    fun executeLogin(email: String, password: String) {
        showLoading(true)
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                loginUseCase.getLogin(email = email, password = password)
            }
            result.either(::handleUseCaseFailureFromBase, ::handleSuccessLogin)
        }
    }

    private fun handleSuccessLogin(login: LoginEntity) {
        showLoading(false)
        if (login.status.equals("success")) {
            viewModelScope.launch {
                _state.emit(UILoginState.OnSuccess)
            }
        }

    }
}