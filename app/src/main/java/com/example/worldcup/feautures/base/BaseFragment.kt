package com.example.worldcup.feautures.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.worldcup.feautures.dialog.LoadingDialog

//@AndroidEntryPoint
abstract class BaseFragment<T: ViewDataBinding, ViewModelType : BaseViewModel<*>> : Fragment() {

    lateinit var binding: T
    private lateinit var rootView: View
    private var myDialog: LoadingDialog? = null
    /**
     * This function associate the xml file to the class.
     * In order to pass it to onCreateView and bind it.
     * @return layout id
     */
    abstract val getLayoutId: Int

    /**
     * Return the binding variable of the XML
     * associated to this class.
     * @return binding variable ID
     */
    abstract val getBindingVariable: Int

    abstract val classTypeOfVM: Class<ViewModelType>
    lateinit var myViewModel: ViewModelType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Binding the viewModel
        myViewModel = ViewModelProvider(this).get(classTypeOfVM)
        if (myViewModel == null) {
            throw Exception("View Model must not be null.")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId, container, false)
        rootView = binding.root
        binding.setVariable(getBindingVariable, myViewModel)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.executePendingBindings()

        myDialog = LoadingDialog()

        if (savedInstanceState == null)
            onFragmentViewReady(view)
            showLoadingDialog()
    }

    abstract fun onFragmentViewReady(view: View)

    /**
     * Add a fragment above the current fragment. Also allows to use the back stack.
     *
     * @param containerId the frameLayout id.
     * @param fragment the fragment to add.
     * @param fromParent if true the we have to use activity?.supportFragmentManager.
     */
    protected fun addFragment(containerId: Int,
                              fragment: Fragment,
                              fromParent: Boolean = false) {
        val fragmentTransaction = if (fromParent) {
            activity?.supportFragmentManager?.beginTransaction()
        } else {
            fragmentManager?.beginTransaction()
        }
        fragmentTransaction?.add(containerId, fragment)
                ?.addToBackStack(null)
                ?.commit()
    }

    private fun showLoadingDialog() {
        myViewModel.isLoading.observe(viewLifecycleOwner) {
            try {
                if (it) {
                    myDialog?.let { dialog ->
                        childFragmentManager.beginTransaction().remove(dialog).commit()
                        if (!dialog.isAdded) {
                            dialog.show(//childFragmentManager,
                                requireActivity().supportFragmentManager,
                                "loading"
                            )//childFragmentManager
                        } else {
                            childFragmentManager.beginTransaction().remove(dialog).commit()
                        }
                    }

                } else if (!it) {
                    myDialog?.dismiss()
                }
            } catch (e: Exception) {
                println(e.localizedMessage)
            }
        }
    }

    fun showToast(msg: String){
        Toast.makeText(requireContext(),msg,Toast.LENGTH_SHORT).show()
    }
}