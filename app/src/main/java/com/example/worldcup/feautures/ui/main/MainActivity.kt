package com.example.worldcup.feautures.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.databinding.ActivityMainBinding
import com.example.worldcup.feautures.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override val getLayoutId: Int
        get() = R.layout.activity_main
    override val getBindingVariable: Int
        get() = BR.mainViewModel
    override val classTypeOfVM: Class<MainViewModel>
        get() = MainViewModel::class.java
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}