package com.example.worldcup.bindingTools

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("setImageViewUrl")
fun setImageViewUrl(view: ImageView, text: String?) {
    text?.let {
        Glide
            .with(view.context)
            .load(text)
            .into(view)
    }
}