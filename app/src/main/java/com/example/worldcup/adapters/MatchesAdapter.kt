package com.example.worldcup.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.worldcup.BR
import com.example.worldcup.R
import com.example.worldcup.databinding.ItemMatchBinding
import com.example.worldcup.feautures.utils.Constants
import com.example.worldcup.model.MatchesModelView

class MatchesAdapter(private val list: MutableList<MatchesModelView>) :
    RecyclerView.Adapter<MatchesAdapter.MatchesHolder>() {


    inner class MatchesHolder(val itemBinding: ItemMatchBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun setItem(item: MatchesModelView) {
            itemBinding.setVariable(BR.matchModel, item)
            itemBinding.executePendingBindings()
            itemBinding.clHome.setOnClickListener {
                val bundle = bundleOf(Constants.KEY_CODE_TEAM to item.home_team_id)
                itemBinding.root.findNavController().navigate(R.id.teamFragment,bundle)
            }
            itemBinding.clAway.setOnClickListener {
                val bundle = bundleOf(Constants.KEY_CODE_TEAM to item.away_team_id)
                itemBinding.root.findNavController().navigate(R.id.teamFragment,bundle)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatchesHolder {
        val binding: ItemMatchBinding? = DataBindingUtil.bind(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_match, parent, false
            )
        )
        return MatchesHolder(binding!!)
    }

    override fun onBindViewHolder(holder: MatchesHolder, position: Int) {
        val match = list[holder.adapterPosition]
        holder.setItem(match)
    }

    fun getPlanByPosition(position: Int): MatchesModelView {
        return list[position]
    }

    override fun getItemCount(): Int {
        return if (list.isNullOrEmpty()) {
            0
        } else {
            list.size
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addItems(newList: List<MatchesModelView>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }

}