package com.example.worldcup.mapper.teams

import com.example.domain.entity.teams.TeamsEntity
import com.example.worldcup.model.TeamsModelView
import javax.inject.Inject

class TeamsModelMapperImpl @Inject constructor() : TeamsModelMapper {
    override suspend fun teamDomainToPresentation(teams: TeamsEntity?): List<TeamsModelView>? {
        return teams?.data?.let { list ->
            list.map { tm ->
                TeamsModelView(
                    id = tm.id.orEmpty(),
                    name_en = tm.name_en.orEmpty(),
                    flag = tm.flag.orEmpty(),
                    fifa_code = tm.fifa_code.orEmpty(),
                    groups = tm.groups.orEmpty()
                )
            }
        }
    }
}