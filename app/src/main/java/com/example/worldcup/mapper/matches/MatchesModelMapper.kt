package com.example.worldcup.mapper.matches

import com.example.domain.entity.matches.MatchesEntity
import com.example.worldcup.model.MatchesModelView

interface MatchesModelMapper {
    suspend fun matchesDomainToPresentation(matches:MatchesEntity?): List<MatchesModelView>?
}