package com.example.worldcup.mapper.matches

import com.example.domain.entity.matches.MatchesEntity
import com.example.worldcup.model.MatchesModelView
import javax.inject.Inject

class MatchesModelMapperImpl @Inject constructor() : MatchesModelMapper {
    override suspend fun matchesDomainToPresentation(matches: MatchesEntity?): List<MatchesModelView>? {
        return matches?.data?.let { list ->
            list.map { mt ->
                MatchesModelView(
                    _id = mt._id.orEmpty(),
                    away_flag = mt.away_flag.orEmpty(),
                    away_score = mt.away_score ?: 0,
                    away_scorers = mt.away_scorers.orEmpty(),
                    away_team_en = mt.away_team_en.orEmpty(),
                    away_team_fa = mt.away_team_fa.orEmpty(),
                    away_team_id = mt.away_team_id.orEmpty(),
                    finished = mt.finished.orEmpty(),
                    group = mt.group.orEmpty(),
                    home_flag = mt.home_flag.orEmpty(),
                    home_scorers = mt.home_scorers.orEmpty(),
                    home_score = mt.home_score?: 0,
                    home_team_en = mt.home_team_en.orEmpty(),
                    home_team_fa = mt.home_team_fa.orEmpty(),
                    home_team_id = mt.home_team_id.orEmpty(),
                    id = mt.id.orEmpty(),
                    local_date = mt.local_date.orEmpty(),
                    matchday = mt.matchday.orEmpty(),
                    persian_date = mt.persian_date.orEmpty(),
                    stadium_id = mt.stadium_id.orEmpty(),
                    time_elapsed = mt.time_elapsed.orEmpty(),
                    type = mt.type.orEmpty()
                )
            }
        }
    }
}