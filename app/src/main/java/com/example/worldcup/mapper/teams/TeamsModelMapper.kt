package com.example.worldcup.mapper.teams

import com.example.domain.entity.teams.TeamsEntity
import com.example.worldcup.model.TeamsModelView

interface TeamsModelMapper {
    suspend fun teamDomainToPresentation(teams:TeamsEntity?):List<TeamsModelView>?
}