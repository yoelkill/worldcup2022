package com.example.framework.usecase.matches

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesByIdRepository
import com.example.domain.usecase.matches.MatchesByIdUseCase
import javax.inject.Inject

class GetMatchesByIdUseCaseImpl @Inject constructor(private val repository: MatchesByIdRepository):MatchesByIdUseCase {
    override suspend fun getMatchesById(id: String): Either<Failure, MatchesEntity> {
        return  repository.getMatchesById(id = id)
    }
}