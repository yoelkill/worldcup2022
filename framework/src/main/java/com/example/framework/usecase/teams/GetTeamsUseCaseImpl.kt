package com.example.framework.usecase.teams

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity
import com.example.domain.repository.TeamsRepository
import com.example.domain.usecase.teams.TeamsUseCase
import javax.inject.Inject

class GetTeamsUseCaseImpl @Inject constructor(private val repository: TeamsRepository) :
    TeamsUseCase {
    override suspend fun getTeams(): Either<Failure, TeamsEntity> = repository.getTeams()
}