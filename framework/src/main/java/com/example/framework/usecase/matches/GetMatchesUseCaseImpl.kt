package com.example.framework.usecase.matches

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesRepository
import com.example.domain.usecase.matches.MatchesUseCase
import javax.inject.Inject

class GetMatchesUseCaseImpl @Inject constructor(private val repository: MatchesRepository) :
    MatchesUseCase {
    override suspend fun getMatches(): Either<Failure, MatchesEntity> {
        return repository.getMatches()
    }
}