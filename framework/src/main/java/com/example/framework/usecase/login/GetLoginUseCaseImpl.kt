package com.example.framework.usecase.login

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.login.LoginEntity
import com.example.domain.repository.LoginRepository
import com.example.domain.usecase.login.LoginUseCase
import javax.inject.Inject

class GetLoginUseCaseImpl @Inject constructor(
    private val repository: LoginRepository
) : LoginUseCase {
    override suspend fun getLogin(email: String, password: String): Either<Failure, LoginEntity> {
        return repository.getLogin(email = email, password = password)
    }
}