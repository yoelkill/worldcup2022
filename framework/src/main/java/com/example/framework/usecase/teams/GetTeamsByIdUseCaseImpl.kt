package com.example.framework.usecase.teams

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity
import com.example.domain.repository.TeamsByIdRepository
import com.example.domain.usecase.teams.TeamsByIdUseCase

import javax.inject.Inject

class GetTeamsByIdUseCaseImpl @Inject constructor(private val repository: TeamsByIdRepository) :
    TeamsByIdUseCase {

    override suspend fun getTeamsById(id: String): Either<Failure, TeamsEntity> =
        repository.getTeamsById(id)
}