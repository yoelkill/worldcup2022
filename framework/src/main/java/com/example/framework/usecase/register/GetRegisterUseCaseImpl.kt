package com.example.framework.usecase.register

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.register.RegisterEntity
import com.example.domain.repository.RegisterRepository
import com.example.domain.usecase.register.RegisterUseCase
import javax.inject.Inject

class GetRegisterUseCaseImpl @Inject constructor(private val repository: RegisterRepository) :
    RegisterUseCase {
    override suspend fun register(
        name: String,
        email: String,
        pwr: String,
        pwrConfirm: String
    ): Either<Failure, RegisterEntity> = repository.getRegister(name, email, pwr, pwrConfirm)
}