package com.example.framework.usecase.matches

import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesByDateRepository
import com.example.domain.usecase.matches.MatchesByDateUseCase
import javax.inject.Inject

class GetMatchesByDateUseCaseImpl @Inject constructor(private val repository: MatchesByDateRepository):MatchesByDateUseCase {
    override suspend fun getMatchesByDate(date: String): Either<Failure, MatchesEntity> {
        return repository.getMatchesByDate(date)
    }
}