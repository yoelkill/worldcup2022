package com.example.framework.di

import com.example.domain.usecase.login.LoginUseCase
import com.example.domain.usecase.matches.MatchesByDateUseCase
import com.example.domain.usecase.matches.MatchesByIdUseCase
import com.example.domain.usecase.matches.MatchesUseCase
import com.example.domain.usecase.register.RegisterUseCase
import com.example.domain.usecase.teams.TeamsByIdUseCase
import com.example.domain.usecase.teams.TeamsUseCase
import com.example.framework.usecase.login.GetLoginUseCaseImpl
import com.example.framework.usecase.matches.GetMatchesByDateUseCaseImpl
import com.example.framework.usecase.matches.GetMatchesByIdUseCaseImpl
import com.example.framework.usecase.matches.GetMatchesUseCaseImpl
import com.example.framework.usecase.register.GetRegisterUseCaseImpl
import com.example.framework.usecase.teams.GetTeamsByIdUseCaseImpl
import com.example.framework.usecase.teams.GetTeamsUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseLogin(
        useCaseImpl: GetLoginUseCaseImpl
    ): LoginUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseMatches(
        useCaseImpl: GetMatchesUseCaseImpl
    ): MatchesUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseMatchesById(
        useCaseImpl: GetMatchesByIdUseCaseImpl
    ): MatchesByIdUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseMatchesByDate(
        useCaseImpl: GetMatchesByDateUseCaseImpl
    ): MatchesByDateUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseTeamsById(
        useCaseImpl: GetTeamsByIdUseCaseImpl
    ): TeamsByIdUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseTeams(
        useCaseImpl: GetTeamsUseCaseImpl
    ): TeamsUseCase

    @ViewModelScoped
    @Binds
    abstract fun bindUseCaseRegister(
        useCaseImpl: GetRegisterUseCaseImpl
    ): RegisterUseCase


}