# Copa Mundial 2022 App

Este proyecto Kotlin se centra en la Copa Mundial de la FIFA 2022 y utiliza una arquitectura 
limpia junto con los potentes Componentes Jetpack de Android para proporcionar una aplicación 
modular, escalable y de alta calidad.

## Descripción General

La Copa 2022 App es una aplicación que brinda a los usuarios información detallada sobre la 
Copa Mundial de la FIFA 2022. Desde la visualización de partidos hasta datos sobre equipos 
y jugadores, esta aplicación ofrece una experiencia para visualizar los matchs y resultados..

## Arquitectura

La arquitectura del proyecto sigue los principios de Clean Architecture, separando las 
capas para garantizar un código organizado y fácilmente mantenible.

* Capa de Presentación (Presentation Layer): 
Implementada utilizando el patrón MVVM, esta capa gestiona la interfaz de usuario y 
utiliza los Componentes Jetpack para la observación de datos y la navegación entre fragmentos.

* Capa de Dominio (Domain Layer): 
Contiene la lógica de negocios relacionada con la Copa Mundial, como la obtención de datos 
de partidos y detalles de equipos. Define casos de uso para la aplicación.

* Capa de Datos (Data Layer): 
Encargada de obtener y almacenar datos, esta capa utiliza 
Room para la persistencia local y Retrofit para acceder a servicios web y obtener 
datos en tiempo real.

## Componentes Jetpack de Android Utilizados


### ViewModel

Los ViewModels se utilizan para gestionar y mantener los datos de la interfaz de usuario 
relacionados con la Copa Mundial. Permiten la gestión eficiente del ciclo de vida
 y proporcionan una comunicación clara entre la capa de presentación y la de dominio.

## LiveData

LiveData es utilizado para notificar cambios en los datos de manera reactiva a la 
interfaz de usuario. Facilita la actualización automática de la interfaz 
de usuario en respuesta a cambios en los datos.

## Retrofit

Retrofit se utiliza para acceder a servicios web y obtener datos en tiempo real, 
como los últimos resultados de los partidos o detalles de los jugadores.

## Navigation

Navigation se utiliza para gestionar la navegación entre diferentes fragmentos de la aplicación,
proporcionando una experiencia de usuario coherente y fácil de entender.

### Requisitos

* Android Studio 4.2 o superior
* Dispositivo o emulador con Android API nivel 21 o superior

### Instalación

* Clona el repositorio: git clone https://gitlab.com/yoelkill/worldcup2022.git
* Abre el proyecto en Android Studio.
* Conecta un dispositivo Android o utiliza un emulador.
* Ejecuta la aplicación (Shift + F10).

## License

For open source projects, say how it is licensed.

## Sources

* [Android Developers][android-developers] 

* [Componentes jetpack][jetpack-components] 

* Libro "Clean Architecture: A Craftsman's Guide to Software Structure and Design" de Robert C. Martin.


[//]: # "Source definitions"
[android-developers]: https://developer.android.com/?hl=es-419 "Android documentation"
[jetpack-components]: https://developer.android.com/jetpack?hl=es-419 "jetpack components"


## Conclusión

La Copa 2022 App es un proyecto emocionante desarrollado en Kotlin que combina la potencia de 
una arquitectura limpia con los versátiles Componentes Jetpack de Android. 
La adopción de Clean Architecture proporciona una estructura modular, 
facilitando el mantenimiento y la expansión del proyecto.
