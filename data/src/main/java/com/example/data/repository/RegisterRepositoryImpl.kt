package com.example.data.repository

import com.example.data.network.mapper.register.RegisterMapper
import com.example.data.network.source.IRegisterEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.register.RegisterEntity
import com.example.domain.repository.RegisterRepository
import javax.inject.Inject

class RegisterRepositoryImpl @Inject constructor(
    private val source: IRegisterEndPoints,
    private val mapper: RegisterMapper
) : RegisterRepository {
    override suspend fun getRegister(
        name: String,
        email: String,
        pwr: String,
        pwrConfirm: String
    ): Either<Failure, RegisterEntity> {
        return when (val response = source.register(name, email, pwr, pwrConfirm)) {
            is Either.Right -> Either.Right(mapper.registerDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}