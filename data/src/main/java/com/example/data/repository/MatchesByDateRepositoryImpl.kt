package com.example.data.repository

import com.example.data.network.mapper.matches.MatchesMapper
import com.example.data.network.source.IMatchesByDateEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesByDateRepository
import javax.inject.Inject

class MatchesByDateRepositoryImpl @Inject constructor(
    private val source: IMatchesByDateEndPoints,
    private val mapper: MatchesMapper
) : MatchesByDateRepository {
    override suspend fun getMatchesByDate(date: String): Either<Failure, MatchesEntity> {
        return when (val response = source.getMatchesByDate(date)) {
            is Either.Right -> Either.Right(mapper.matchesDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}