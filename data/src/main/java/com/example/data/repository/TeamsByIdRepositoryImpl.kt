package com.example.data.repository

import com.example.data.network.mapper.teams.TeamsMapper
import com.example.data.network.source.ITeamsByIdEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity
import com.example.domain.repository.TeamsByIdRepository

import javax.inject.Inject

class TeamsByIdRepositoryImpl @Inject constructor(
    private val source: ITeamsByIdEndPoints,
    private val mapper: TeamsMapper
):TeamsByIdRepository {
    override suspend fun getTeamsById(id: String): Either<Failure, TeamsEntity> {
        return when (val response = source.getTeamsById(id)) {
            is Either.Right -> Either.Right(mapper.teamsDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}