package com.example.data.repository

import com.example.data.network.mapper.teams.TeamsMapper
import com.example.data.network.source.ITeamsEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.teams.TeamsEntity
import com.example.domain.repository.TeamsRepository
import javax.inject.Inject

class TeamsRepositoryImpl @Inject constructor(
    private val source: ITeamsEndPoints,
    private val mapper: TeamsMapper
) : TeamsRepository {
    override suspend fun getTeams(): Either<Failure, TeamsEntity> {
        return when (val response = source.getTeams()) {
            is Either.Right -> Either.Right(mapper.teamsDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}