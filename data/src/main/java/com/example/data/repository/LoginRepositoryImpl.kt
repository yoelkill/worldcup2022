package com.example.data.repository

import com.example.data.network.mapper.login.LoginMapper
import com.example.data.network.source.ILoginEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.login.LoginEntity
import com.example.domain.repository.LoginRepository
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val source: ILoginEndPoints,
    private val mapper: LoginMapper
) : LoginRepository {
    override suspend fun getLogin(email: String, password: String): Either<Failure, LoginEntity> {
        return when (val response = source.getLogin(email, password)) {
            is Either.Right -> Either.Right(mapper.loginDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}