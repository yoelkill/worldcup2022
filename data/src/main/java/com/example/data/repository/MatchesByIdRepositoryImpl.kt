package com.example.data.repository

import com.example.data.network.mapper.matches.MatchesMapper
import com.example.data.network.source.IMatchesByIdEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesByIdRepository
import javax.inject.Inject

class MatchesByIdRepositoryImpl @Inject constructor(
    private val source: IMatchesByIdEndPoints,
    private val mapper: MatchesMapper
):MatchesByIdRepository {
    override suspend fun getMatchesById(id: String): Either<Failure, MatchesEntity> {
        return when (val response = source.getMatchesById(id)) {
            is Either.Right -> Either.Right(mapper.matchesDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}