package com.example.data.repository

import com.example.data.network.mapper.matches.MatchesMapper
import com.example.data.network.source.IMatchesEndPoints
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import com.example.domain.entity.matches.MatchesEntity
import com.example.domain.repository.MatchesRepository
import javax.inject.Inject

class MatchesRepositoryImpl @Inject constructor(
    private val source: IMatchesEndPoints,
    private val mapper: MatchesMapper
) : MatchesRepository {
    override suspend fun getMatches(): Either<Failure, MatchesEntity> {
        return when (val response = source.getMatches()) {
            is Either.Right -> Either.Right(mapper.matchesDataToDomain(response.b))
            is Either.Left -> Either.Left(response.a)
        }
    }
}