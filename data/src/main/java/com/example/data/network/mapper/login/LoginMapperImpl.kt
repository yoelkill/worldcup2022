package com.example.data.network.mapper.login

import com.example.data.entity.login.LoginResponse
import com.example.data.preferences.SecurePreferences
import com.example.domain.entity.login.LoginDataEntity
import com.example.domain.entity.login.LoginEntity
import javax.inject.Inject

class LoginMapperImpl @Inject constructor(
    private val preferences: SecurePreferences
) : LoginMapper {
    override suspend fun loginDataToDomain(data: LoginResponse): LoginEntity {
        return LoginEntity(
            data = data.data?.let { dt ->
                preferences.saveToken(dt.token.orEmpty())
                LoginDataEntity(token = dt.token)
            },
            status = data.status
        )
    }
}