package com.example.data.network
import com.example.data.preferences.SecurePreferences
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class SupportInterceptor @Inject constructor(
    private val preferences : SecurePreferences
) : Interceptor {

    /**
     * Interceptor class for setting of the headers for every requests
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Accept", "application/json")
            //.addHeader("token", preferences.getAccesToken())
            .build()
        return chain.proceed(request)
    }
}