package com.example.data.network.source

import com.example.data.entity.matches.MatchesResponse
import com.example.data.network.end_points.Matches
import com.example.data.network.remote.NetworkHandler
import com.example.data.network.utils.UtilDefaultHeader
import com.example.data.preferences.SecurePreferences
import com.example.data.util.DBConstant
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

interface IMatchesEndPoints {
    suspend fun getMatches(): Either<Failure, MatchesResponse>
    class IMarchesEndPointsImpl @Inject constructor(
        @Named(DBConstant.REST_RETROFIT)
        private val retrofit: Retrofit,
        private val networkHandler: NetworkHandler,
        private val preferences: SecurePreferences
    ) : IMatchesEndPoints {
        private val api by lazy { retrofit.create(Matches::class.java) }
        override suspend fun getMatches(): Either<Failure, MatchesResponse> =
            networkHandler.callService {
                api.getMatches(header = UtilDefaultHeader.getDefaultHeader(preferences.getToken()))
            }
    }
}