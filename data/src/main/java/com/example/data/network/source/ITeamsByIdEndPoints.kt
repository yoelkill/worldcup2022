package com.example.data.network.source

import com.example.data.entity.teams.TeamsResponse
import com.example.data.network.end_points.Teams
import com.example.data.network.remote.NetworkHandler
import com.example.data.network.utils.UtilDefaultHeader
import com.example.data.preferences.SecurePreferences
import com.example.data.util.DBConstant
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

interface ITeamsByIdEndPoints {
    suspend fun getTeamsById(id: String): Either<Failure, TeamsResponse>
    class ITeamsByIdEndPointsImpl @Inject constructor(
        @Named(DBConstant.REST_RETROFIT)
        private val retrofit: Retrofit,
        private val networkHandler: NetworkHandler,
        private val preferences: SecurePreferences
    ) : ITeamsByIdEndPoints {
        private val api by lazy { retrofit.create(Teams::class.java) }
        override suspend fun getTeamsById(id: String): Either<Failure, TeamsResponse> =
            networkHandler.callService {
                api.getTeamsById(
                    header = UtilDefaultHeader.getDefaultHeader(preferences.getToken())
                )
            }
    }
}