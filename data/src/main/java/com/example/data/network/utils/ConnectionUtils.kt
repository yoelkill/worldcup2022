package com.example.data.network.utils

interface ConnectionUtils {
    fun isNetworkAvailable(): Boolean
}