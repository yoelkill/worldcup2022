package com.example.data.network.end_points

import com.example.data.entity.login.LoginResponse
import com.example.data.entity.login.request.LoginRequest
import com.example.data.entity.matches.MatchesResponse
import com.example.data.entity.matches.request.MatchesByDateRequest
import com.example.data.entity.register.RegisterResponse
import com.example.data.entity.register.request.RegisterRequest
import com.example.data.entity.teams.TeamsResponse
import retrofit2.Response
import retrofit2.http.*


interface Login {
    @POST("user/login")
    suspend fun login(
        @Body body: LoginRequest
    ): Response<LoginResponse>
}

interface Matches {
    @GET("match")
    suspend fun getMatches(@HeaderMap header: Map<String, String>): Response<MatchesResponse>
    @GET("match/8")
    suspend fun getMatchesById(@HeaderMap header: Map<String, String>): Response<MatchesResponse>
    @POST("bydate")
    suspend fun getMatchesByDate(@HeaderMap header: Map<String, String>,@Body body: MatchesByDateRequest): Response<MatchesResponse>
}

interface Teams {
    @GET("team")
    suspend fun getTeams(@HeaderMap header: Map<String, String>) :Response<TeamsResponse>
    @GET("team/6")
    suspend fun getTeamsById(@HeaderMap header: Map<String, String>): Response<TeamsResponse>
}

interface Register{
    @POST("user")
    suspend fun register(
        @Body body: RegisterRequest
    ): Response<RegisterResponse>
}