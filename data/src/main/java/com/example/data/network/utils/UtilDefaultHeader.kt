package com.example.data.network.utils

object UtilDefaultHeader {
    fun getDefaultHeader(token: String): HashMap<String, String> {
        val myHeader = HashMap<String, String>()
        myHeader["Authorization"] = "Bearer $token"
        return myHeader
    }
}