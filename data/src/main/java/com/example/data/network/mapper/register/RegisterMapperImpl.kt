package com.example.data.network.mapper.register

import com.example.data.entity.register.RegisterResponse
import com.example.data.preferences.SecurePreferences
import com.example.domain.entity.register.RegisterDataEntity
import com.example.domain.entity.register.RegisterEntity
import javax.inject.Inject

class RegisterMapperImpl @Inject constructor(
    private val preferences: SecurePreferences
) : RegisterMapper {
    override suspend fun registerDataToDomain(data: RegisterResponse): RegisterEntity {
        return RegisterEntity(
            data = data.data?.let { rr ->
                preferences.saveToken(rr.token.orEmpty())
                RegisterDataEntity(token = rr?.token)
            }, status = data.status, message = data.message
        )
    }
}