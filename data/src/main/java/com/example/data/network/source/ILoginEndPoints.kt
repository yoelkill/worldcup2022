package com.example.data.network.source

import com.example.data.entity.login.LoginResponse
import com.example.data.entity.login.request.LoginRequest
import com.example.data.network.end_points.Login
import com.example.data.network.remote.NetworkHandler
import com.example.data.util.DBConstant
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

interface ILoginEndPoints {
    suspend fun getLogin(email: String, password: String): Either<Failure, LoginResponse>

    class ILoginEndPointsImpl @Inject constructor(
        @Named(DBConstant.REST_RETROFIT)
        private val retrofit: Retrofit,
        private val networkHandler: NetworkHandler
    ) : ILoginEndPoints {
        private val api by lazy { retrofit.create(Login::class.java) }

        override suspend fun getLogin(
            email: String,
            password: String
        ): Either<Failure, LoginResponse> = networkHandler.callService {
            val dataLoginRequest = LoginRequest(email = email, password = password)
            api.login(dataLoginRequest)
        }

    }
}