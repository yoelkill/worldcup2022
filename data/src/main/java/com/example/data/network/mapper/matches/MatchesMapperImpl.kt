package com.example.data.network.mapper.matches

import com.example.data.entity.matches.MatchesResponse
import com.example.domain.entity.matches.MatchesDataEntity
import com.example.domain.entity.matches.MatchesEntity
import javax.inject.Inject

class MatchesMapperImpl @Inject constructor() : MatchesMapper {
    override suspend fun matchesDataToDomain(data: MatchesResponse): MatchesEntity {
        return MatchesEntity(data = data.data?.let { dt ->
            dt.map { mtc ->
                MatchesDataEntity(
                    _id = mtc.id_code,
                    away_flag = mtc.awayFlag,
                    away_score = mtc.awayScore,
                    away_scorers = mtc.awayScorers,
                    away_team_en = mtc.awayTeamEn,
                    away_team_fa = mtc.awayTeamFa,
                    away_team_id = mtc.awayTeamId,
                    finished = mtc.finished,
                    group = mtc.group,
                    home_flag = mtc.homeFlag,
                    home_score = mtc.homeScore,
                    home_scorers = mtc.homeScorers,
                    home_team_en = mtc.homeTeamEn,
                    home_team_fa = mtc.homeTeamFa,
                    home_team_id = mtc.homeTeamId,
                    id = mtc.id,
                    local_date = mtc.localDate,
                    matchday = mtc.matchday,
                    persian_date = mtc.persianDate,
                    stadium_id = mtc.stadiumId,
                    time_elapsed = mtc.timeElapsed,
                    type = mtc.type
                )
            }
        }, status = data.status)
    }
}