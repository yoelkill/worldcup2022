package com.example.data.network.mapper.login


import com.example.data.entity.login.LoginResponse
import com.example.domain.entity.login.LoginEntity

interface LoginMapper {
    suspend fun loginDataToDomain(data: LoginResponse): LoginEntity
}