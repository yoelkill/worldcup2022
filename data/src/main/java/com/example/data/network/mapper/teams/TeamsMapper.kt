package com.example.data.network.mapper.teams

import com.example.data.entity.teams.TeamsResponse
import com.example.domain.entity.teams.TeamsEntity

interface TeamsMapper {
    suspend fun teamsDataToDomain(data:TeamsResponse):TeamsEntity
}