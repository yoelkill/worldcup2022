package com.example.data.network.mapper.register

import com.example.data.entity.register.RegisterResponse
import com.example.domain.entity.register.RegisterEntity


interface RegisterMapper {
    suspend fun registerDataToDomain(data:RegisterResponse):RegisterEntity
}