package com.example.data.network.mapper.teams

import com.example.data.entity.teams.TeamsResponse
import com.example.domain.entity.teams.TeamsDataEntity
import com.example.domain.entity.teams.TeamsEntity
import javax.inject.Inject

class TeamsMapperImpl @Inject constructor() : TeamsMapper {
    override suspend fun teamsDataToDomain(data: TeamsResponse): TeamsEntity {
        return TeamsEntity(data = data.data?.let { dt ->
            dt.map { tm ->
                TeamsDataEntity(
                    _id = tm.id_code,
                    fifa_code = tm.fifaCode,
                    flag = tm.flag,
                    groups = tm.groups,
                    id = tm.id,
                    iso2 = tm.iso2,
                    name_en = tm.nameEn,
                    name_fa = tm.nameFa
                )
            }
        }, status = data.status)
    }
}