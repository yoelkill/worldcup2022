package com.example.data.network.mapper.matches

import com.example.data.entity.matches.MatchesResponse
import com.example.domain.entity.matches.MatchesEntity

interface MatchesMapper {
    suspend fun matchesDataToDomain(data: MatchesResponse): MatchesEntity
}