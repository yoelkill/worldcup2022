package com.example.data.network.source

import com.example.data.entity.teams.TeamsResponse
import com.example.data.network.end_points.Matches
import com.example.data.network.end_points.Teams
import com.example.data.network.remote.NetworkHandler
import com.example.data.network.utils.UtilDefaultHeader
import com.example.data.preferences.SecurePreferences
import com.example.data.util.DBConstant
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

interface ITeamsEndPoints {
    suspend fun getTeams(): Either<Failure, TeamsResponse>
    class ITeamsEndPointsImpl @Inject constructor(
        @Named(DBConstant.REST_RETROFIT)
        private val retrofit: Retrofit,
        private val networkHandler: NetworkHandler,
        private val preferences: SecurePreferences
    ) : ITeamsEndPoints {
        private val api by lazy { retrofit.create(Teams::class.java) }
        override suspend fun getTeams(): Either<Failure, TeamsResponse> =
            networkHandler.callService {
                api.getTeams(header = UtilDefaultHeader.getDefaultHeader(preferences.getToken()))
            }
    }

}