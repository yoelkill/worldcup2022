package com.example.data.network.source

import com.example.data.entity.register.RegisterResponse
import com.example.data.entity.register.request.RegisterRequest
import com.example.data.network.end_points.Register
import com.example.data.network.remote.NetworkHandler
import com.example.data.util.DBConstant
import com.example.domain.entity.Either
import com.example.domain.entity.Failure
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

interface IRegisterEndPoints {
    suspend fun register(
        name: String,
        email: String,
        pwr: String,
        pwrConfirm: String
    ): Either<Failure, RegisterResponse>

    class IRegisterEndPointsImpl @Inject constructor(
        @Named(DBConstant.REST_RETROFIT)
        private val retrofit: Retrofit,
        private val networkHandler: NetworkHandler,
    ) : IRegisterEndPoints {
        private val api by lazy { retrofit.create(Register::class.java) }
        override suspend fun register(
            name: String,
            email: String,
            pwr: String,
            pwrConfirm: String
        ): Either<Failure, RegisterResponse> = networkHandler.callService {
            val registerRequest = RegisterRequest(email, name, pwr, pwrConfirm)
            api.register(registerRequest)
        }
    }
}