package com.example.data.preferences
import android.content.SharedPreferences
import com.example.data.preferences.PrefsConstant.PREF_KEY_NAME
import com.example.data.preferences.PrefsConstant.PREF_TOKEN
import javax.inject.Inject

class SecurePreferencesImpl @Inject constructor(private val prefs: SharedPreferences) :
    SecurePreferences {
    override fun saveName(name: String) {
        prefs.edit().putString(PREF_KEY_NAME, name).apply()
    }

    override fun getName(): String {
        return prefs.getString(PREF_KEY_NAME, "") ?: ""
    }

    override fun saveToken(token: String) {
        prefs.edit().putString(PREF_TOKEN, token).apply()
    }

    override fun getToken(): String {
        return prefs.getString(PREF_TOKEN, "") ?: ""
    }


    override fun deletePreferences() {
        prefs.edit().clear().apply()
    }
}