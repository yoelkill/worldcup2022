package com.example.data.preferences

object PrefsConstant {
    const val PREF_KEY_NAME : String = "NAME"
    const val PREF_TOKEN : String = "pref_token"
}