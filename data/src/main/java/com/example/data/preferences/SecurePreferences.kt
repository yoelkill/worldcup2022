package com.example.data.preferences

interface SecurePreferences {
    fun saveName(name: String)
    fun getName(): String
    fun saveToken(token: String)
    fun getToken():String
    fun deletePreferences()
}