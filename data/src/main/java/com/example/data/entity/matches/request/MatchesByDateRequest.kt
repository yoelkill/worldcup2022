package com.example.data.entity.matches.request


import com.google.gson.annotations.SerializedName

data class MatchesByDateRequest(
    @SerializedName("date")
    val date: String?
)