package com.example.data.entity.teams


import com.google.gson.annotations.SerializedName

data class TeamsResponse(
    @SerializedName("data")
    val data: List<TeamsDataResponse>?,
    @SerializedName("status")
    val status: String?
)