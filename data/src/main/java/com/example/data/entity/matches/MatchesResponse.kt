package com.example.data.entity.matches


import com.google.gson.annotations.SerializedName

data class MatchesResponse(
    @SerializedName("data")
    val data: List<MatchesDataResponse>?,
    @SerializedName("status")
    val status: String?
)