package com.example.data.entity.register


import com.google.gson.annotations.SerializedName

data class RegisterDataResponse(
    @SerializedName("token")
    val token: String?
)