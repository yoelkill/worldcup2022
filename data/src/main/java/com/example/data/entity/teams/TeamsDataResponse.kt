package com.example.data.entity.teams


import com.google.gson.annotations.SerializedName

data class TeamsDataResponse(
    @SerializedName("fifa_code")
    val fifaCode: String?,
    @SerializedName("flag")
    val flag: String?,
    @SerializedName("groups")
    val groups: String?,
    @SerializedName("_id")
    val id_code: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("iso2")
    val iso2: String?,
    @SerializedName("name_en")
    val nameEn: String?,
    @SerializedName("name_fa")
    val nameFa: String?
)