package com.example.data.entity.register


import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    @SerializedName("data")
    val data: RegisterDataResponse?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("status")
    val status: String?
)