package com.example.data.entity.matches


import com.google.gson.annotations.SerializedName

data class MatchesDataResponse(
    @SerializedName("away_flag")
    val awayFlag: String?,
    @SerializedName("away_score")
    val awayScore: Int?,
    @SerializedName("away_scorers")
    val awayScorers: List<String?>?,
    @SerializedName("away_team_en")
    val awayTeamEn: String?,
    @SerializedName("away_team_fa")
    val awayTeamFa: String?,
    @SerializedName("away_team_id")
    val awayTeamId: String?,
    @SerializedName("finished")
    val finished: String?,
    @SerializedName("group")
    val group: String?,
    @SerializedName("home_flag")
    val homeFlag: String?,
    @SerializedName("home_score")
    val homeScore: Int?,
    @SerializedName("home_scorers")
    val homeScorers: List<String?>?,
    @SerializedName("home_team_en")
    val homeTeamEn: String?,
    @SerializedName("home_team_fa")
    val homeTeamFa: String?,
    @SerializedName("home_team_id")
    val homeTeamId: String?,
    @SerializedName("_id")
    val id_code: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("local_date")
    val localDate: String?,
    @SerializedName("matchday")
    val matchday: String?,
    @SerializedName("persian_date")
    val persianDate: String?,
    @SerializedName("stadium_id")
    val stadiumId: String?,
    @SerializedName("time_elapsed")
    val timeElapsed: String?,
    @SerializedName("type")
    val type: String?
)