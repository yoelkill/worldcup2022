package com.example.data.entity.login


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("data")
    val data: LoginDataResponse?,
    @SerializedName("status")
    val status: String?
)