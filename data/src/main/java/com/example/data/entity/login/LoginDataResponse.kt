package com.example.data.entity.login


import com.google.gson.annotations.SerializedName

data class LoginDataResponse(
    @SerializedName("token")
    val token: String?
)