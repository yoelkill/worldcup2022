package com.example.data.di

import com.example.data.network.mapper.login.LoginMapper
import com.example.data.network.mapper.login.LoginMapperImpl
import com.example.data.network.mapper.matches.MatchesMapper
import com.example.data.network.mapper.matches.MatchesMapperImpl
import com.example.data.network.mapper.register.RegisterMapper
import com.example.data.network.mapper.register.RegisterMapperImpl
import com.example.data.network.mapper.teams.TeamsMapper
import com.example.data.network.mapper.teams.TeamsMapperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class MapperModule {
    @Binds
    abstract fun bindMapperLogin(
        mapperImpl: LoginMapperImpl
    ): LoginMapper

    @Binds
    abstract fun binMapperMatches(mapperImpl: MatchesMapperImpl): MatchesMapper

    @Binds
    abstract fun bindMapperTeams(mapperImpl: TeamsMapperImpl): TeamsMapper

    @Binds
    abstract fun bindMapperRegister(mapperImpl: RegisterMapperImpl): RegisterMapper
}