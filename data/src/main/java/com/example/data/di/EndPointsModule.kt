package com.example.data.di

import com.example.data.network.source.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class EndPointsModule {

    @Binds
    abstract fun bindEndPointLogin(
        endpoint: ILoginEndPoints.ILoginEndPointsImpl
    ): ILoginEndPoints

    @Binds
    abstract fun bindEndPointMatches(
        endpoint: IMatchesEndPoints.IMarchesEndPointsImpl
    ): IMatchesEndPoints

    @Binds
    abstract fun bindEndPointMatchesById(
        endpoint: IMatchesByIdEndPoints.IMatchesByIdEndPointsImpl
    ): IMatchesByIdEndPoints

    @Binds
    abstract fun bindEndPointMatchesByDate(
        endPoint: IMatchesByDateEndPoints.IMatchesByDateEndPointsImpl
    ): IMatchesByDateEndPoints

    @Binds
    abstract fun bindEndPointTeams(
        endPoint: ITeamsEndPoints.ITeamsEndPointsImpl
    ): ITeamsEndPoints

    @Binds
    abstract fun bindEndPointTeamsById(endPoint: ITeamsByIdEndPoints.ITeamsByIdEndPointsImpl): ITeamsByIdEndPoints

    @Binds
    abstract fun bindEndPointRegister(endPoint: IRegisterEndPoints.IRegisterEndPointsImpl): IRegisterEndPoints
}