package com.example.data.di

import com.example.data.repository.*
import com.example.domain.repository.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindRepositoryLogin(
        repositoryImpl: LoginRepositoryImpl
    ): LoginRepository

    @Binds
    abstract fun bindRepositoryMatches(
        repositoryImpl: MatchesRepositoryImpl
    ): MatchesRepository

    @Binds
    abstract fun bindRepositoryMatchesById(
        repositoryImpl: MatchesByIdRepositoryImpl
    ): MatchesByIdRepository

    @Binds
    abstract fun bindRepositoryMatchesByDate(
        repositoryImpl: MatchesByDateRepositoryImpl
    ): MatchesByDateRepository

    @Binds
    abstract fun bindRepositoryTeam(
        repositoryImpl: TeamsRepositoryImpl
    ): TeamsRepository

    @Binds
    abstract fun bindRepositoryTeamById(
        repositoryImpl: TeamsByIdRepositoryImpl
    ): TeamsByIdRepository

    @Binds
    abstract fun bindRepositoryRegister(
        repositoryImpl: RegisterRepositoryImpl
    ): RegisterRepository
}