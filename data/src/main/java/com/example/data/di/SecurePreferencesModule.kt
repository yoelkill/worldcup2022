package com.example.data.di

import com.example.data.preferences.SecurePreferences
import com.example.data.preferences.SecurePreferencesImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class SecurePreferencesModule {
    @Binds
    abstract fun bindSecurePreferences(
        prefs: SecurePreferencesImpl
    ) : SecurePreferences
}